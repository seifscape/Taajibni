//
//  ViewController.h
//  Taajibni
//
//  Created by Seif Kobrosly on 12/27/14.
//  Copyright (c) 2014 Seif Kobrosly. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController


@property (nonatomic, strong) IBOutlet UIButton *loginButton;

@end

